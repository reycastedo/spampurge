#!/bin/bash

# Variables
nombreServicio=spampurge
rutaBinario=/usr/sbin
rutaServicio=/etc/rc.d/init.d
scriptsRaiola=/Raiola/scripts
bashScript=r-borrar_cola.sh

# Comprobaciones
if [[ $(whoami) != root ]]; then
    echo "El servicio necesita ser instalado como root, para poder abrir sockets"
    exit 1
fi

if ! grep CentOS /etc/centos-release | grep -q 6; then
    echo "Script pensado para CentOS 6, no está probado en otro sistema operativo"
    exit 1
fi

if [[ -f $rutaServicio/$nombreServicio ]]; then
    echo "Ya existe un servicio con el nombre $nombreServicio"
    exit 1
fi

if [[ -f $rutaBinario/$nombreServicio ]]; then
    echo "Ya existe un binario llamado $nombreServicio"
    exit 1
fi

# Instalación
! [[ -d $scriptsRaiola ]] && mkdir -p $scriptsRaiola
cp bash_scripts/$bashScript $scriptsRaiola/$bashScript
chown root:root $scriptsRaiola/$bashScript
chmod 755 $scriptsRaiola/$bashScript

cp bin/$nombreServicio $rutaBinario/$nombreServicio
chown root:root $rutaBinario/$nombreServicio
chmod 755 $rutaBinario/$nombreServicio

cp init/$nombreServicio $rutaServicio/$nombreServicio
chown root:root $rutaServicio/$nombreServicio
chmod 755 $rutaServicio/$nombreServicio

cp cron/$nombreServicio /etc/cron.d/$nombreServicio
chown root:root /etc/cron.d/$nombreServicio
chmod 644  /etc/cron.d/$nombreServicio

cp logrotate/$nombreServicio /etc/logrotate.d/$nombreServicio
chown root:root /etc/logrotate.d/$nombreServicio
chmod 755  /etc/logrotate.d/$nombreServicio

chkconfig --add --levels 235 $nombreServicio

if ! service $nombreServicio start; then
    echo "Fallo al arrancar el servicio, revisar manualmente"
    exit 1
fi

exit 0
