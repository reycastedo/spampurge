#!/bin/bash
##Autor: Diego Rodríguez Vázquez
##Fecha: 17/07/2019
##Raiola Networks

log_file="/var/log/r-borrar_cola.log"
timestamp=$(date +"[%Y/%m/%d - %H:%M:%S-%Z]")
ids_cola=$(exiqgrep -i)

## Colores
colorear(){
  LGREEN="\033[1;32m"
  LRED="\033[1;31m"
  LYELLOW="\033[1;33m"
  LGRAY="\033[1;30m"
  LBLUE="\033[1;36m"
  LVIOLET="\033[1;34m"
  LPINK="\033[1;35m"
  NORMAL="\033[m"

  color=\$${1:-NORMAL}

  echo -ne "$(eval echo "${color}")"
  cat

  echo -ne "${NORMAL}"
}

#Función para el borrado de colas de una cuenta smtp
f_smtp(){
  for id in $ids_cola; do
   auth_id=$(exim -Mvh "$id" | grep -o "authenticated_id: $1$")                                             
    if [[ "$auth_id" == "authenticated_id: $1" ]]; then
      exim -Mrm "$id"
    fi
  done
}

#Función para el borrado de colas de una cuenta de usuario
f_mail(){
  for id in $ids_cola; do
   auth_id=$(exim -Mvh "$id" | grep -o "authenticated_id: $1/")                                             
    if [[ "$auth_id" == "authenticated_id: $1/" ]]; then                                                    
      exim -Mrm "$id"
    fi
  done
}

####################################################

#Comprueba las variables introducidas y a partir de estas ejecuta la función correspondiente
case $1 in
  --help)
    echo "Script para el borrado de todos los correos en cola de la cuenta de correo o cpanel indicada"
    echo "Uso: r-borrar_cola.sh <usuario>|<cuenta_correo>";;
  *@*)
    f_smtp "$1"
    echo "$timestamp - r-borrar_cola.sh $*" >> $log_file;;
  *)
    f_mail "$1"
    echo "$timestamp - r-borrar_cola.sh $*" >> $log_file;;
esac
