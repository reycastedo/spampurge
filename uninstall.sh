#!/bin/bash

# Variables
nombreServicio=spampurge
rutaBinario=/usr/sbin
rutaServicio=/etc/rc.d/init.d


# Comprobaciones
if [[ $(whoami) != root ]]; then
    echo "El servicio necesita ser instalado como root, para poder abrir sockets"
    exit 1
fi

if ! [[ -f $rutaServicio/$nombreServicio ]]; then
    echo "No existe un servicio con el nombre $nombreServicio"
    exit 1
fi

if ! [[ -f $rutaBinario/$nombreServicio ]]; then
    echo "No existe un binario llamado $nombreServicio"
    exit 1
fi

# Desactivación y desinstalacon

rm -f /etc/cron.d/$nombreServicio
rm -f /etc/logrotate.d/$nombreServicio
service $nombreServicio stop
chkconfig $nombreServicio off
chkconfig --del $nombreServicio
rm -f $rutaServicio/$nombreServicio
rm -f $rutaBinario/$nombreServicio

exit 0
